package pl.edu.pwsztar.service.impl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.MoveService;

import java.util.Arrays;
import java.util.List;

@Service
public class MoveServiceImpl  implements MoveService {

    @Override
    public Boolean canBishopMove(FigureMoveDto figureMoveDto) {
        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> destinationPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        return Math.abs(mapToInt(startPosition.get(0)) - mapToInt(destinationPosition.get(0)))
                == Math.abs(Integer.parseInt(startPosition.get(1)) - Integer.parseInt(destinationPosition.get(1)));
    }

    private Integer mapToInt(String s) {
        return s.toLowerCase().getBytes()[0] - 97;
    }
}
